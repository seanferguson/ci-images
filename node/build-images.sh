#!/usr/bin/env bash

docker buildx build --push --platform "linux/amd64,linux/arm64,linux/arm" 14.15 -t registry.gitlab.com/seanferguson/ci-images/node:14.15.4  -t registry.gitlab.com/seanferguson/ci-images/node:14.15  -t registry.gitlab.com/seanferguson/ci-images/node:lts
docker buildx build --push --platform "linux/amd64,linux/arm64,linux/arm" 14.15/browsers -t registry.gitlab.com/seanferguson/ci-images/node:14.15.4-browsers  -t registry.gitlab.com/seanferguson/ci-images/node:14.15-browsers  -t registry.gitlab.com/seanferguson/ci-images/node:lts-browsers
docker buildx build --push --platform "linux/amd64,linux/arm64,linux/arm" 15.5 -t registry.gitlab.com/seanferguson/ci-images/node:15.5.1  -t registry.gitlab.com/seanferguson/ci-images/node:15.5  -t registry.gitlab.com/seanferguson/ci-images/node:current
docker buildx build --push --platform "linux/amd64,linux/arm64,linux/arm" 15.5/browsers -t registry.gitlab.com/seanferguson/ci-images/node:15.5.1-browsers  -t registry.gitlab.com/seanferguson/ci-images/node:15.5-browsers  -t registry.gitlab.com/seanferguson/ci-images/node:current-browsers
